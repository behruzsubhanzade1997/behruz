package az.ingress.project.services;

public class Service1Proxy extends Service1 {
    public void sayHello(){
        System.out.println("Proxy in action");
        super.sayHello();
        System.out.println("Proxy complted");
    }
}
