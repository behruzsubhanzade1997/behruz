package az.ingress.project.services;

public interface Chargable {
    void charge (Double amount);
}
