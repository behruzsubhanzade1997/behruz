package az.ingress.project.services;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
@Slf4j
@Aspect
@Component
public class ExecutionTimeLogger {
    @Pointcut("execution(* az.ingress.project.services.HelloServiceImpl.*(..))")
    private void logExecutionTimePc(){

    }
    @SneakyThrows
    @Around(value = "logExecutionTimePc()")
    public void logExecutionTime(ProceedingJoinPoint jp){
        long started = System.currentTimeMillis();
        jp.proceed();
        long ended = System.currentTimeMillis();
        log.info("Elapsed time {}" , ended - started);

    }

//    @Before(value = "logExecutionTimePc()")
//    public void logExecutionTime(){
//        log.info("Our ascept in action");
//
//    }
}
