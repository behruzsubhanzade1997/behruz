package az.ingress.project.services;

public interface DriveLicenseValidator {

    boolean isValid(DriveLicenseDto dto);

}
