package az.ingress.project.services;

import az.ingress.project.controller.Dto;

public interface HelloService {
    Dto sayHello();
    Dto sayHello2();
    Dto sayHello3();
}
