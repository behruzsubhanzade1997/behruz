package az.ingress.project.services;

import lombok.Data;

@Data
public class CreditCardDto {
    String cardId;
}
