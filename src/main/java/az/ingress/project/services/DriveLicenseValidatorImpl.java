package az.ingress.project.services;

import az.ingress.project.services.validators.AbstractValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DriveLicenseValidatorImpl implements DriveLicenseValidator {

    private final List<AbstractValidator> validators;
    public boolean isValid(DriveLicenseDto dto) {
     for (AbstractValidator abstractValidator : validators) {
         boolean valid1 = abstractValidator.isValid(dto);
         if (!valid1)
             return false;
     }
     return true;
    }

}
