package az.ingress.project.services.validators;

import az.ingress.project.services.DriveLicenseDto;
import org.springframework.stereotype.Component;

@Component
public class CategoryValidator implements AbstractValidator {
    @Override
    public boolean isValid(DriveLicenseDto dto) {
        if (dto.getCategory().equals("B")){
            return false;
        }
        return true;
    }
}
