package az.ingress.project.services.validators;

import az.ingress.project.services.DriveLicenseDto;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BlackListValidator implements AbstractValidator {

    private List blackLiist = List.of(1l,2l,3l);
    @Override
    public boolean isValid(DriveLicenseDto dto) {
        boolean contains = blackLiist.contains(dto.getId());
       if (contains)
           return false;
       return true;
        }
    }

