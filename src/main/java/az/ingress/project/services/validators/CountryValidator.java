package az.ingress.project.services.validators;

import az.ingress.project.services.DriveLicenseDto;
import org.springframework.stereotype.Component;

@Component
public class CountryValidator implements AbstractValidator {

    @Override
    public boolean isValid(DriveLicenseDto dto) {
        if (dto.getCountry().equals("Russia")) {
            return false;

        } return true;
    }
}
