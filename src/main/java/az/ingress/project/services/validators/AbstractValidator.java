package az.ingress.project.services.validators;

import az.ingress.project.services.DriveLicenseDto;

public interface AbstractValidator {

    boolean isValid(DriveLicenseDto dto);
}
