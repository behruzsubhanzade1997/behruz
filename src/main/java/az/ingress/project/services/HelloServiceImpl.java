package az.ingress.project.services;

import az.ingress.project.controller.Dto;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

@Slf4j
@Service
public class HelloServiceImpl implements HelloService {
    @SneakyThrows
    public Dto sayHello(){
        Thread.sleep(new Random().nextInt(1000));
        return new Dto("Hello world");
    }

    @Override
    @SneakyThrows
    public Dto sayHello2() {
        Thread.sleep(new Random().nextInt(1000));
        return new Dto("Hello world");
    }

    @Override
    @SneakyThrows
    public Dto sayHello3() {
        Thread.sleep(new Random().nextInt(1000));
        return new Dto("Hello world");
    }
}
