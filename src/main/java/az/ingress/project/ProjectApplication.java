package az.ingress.project;

import az.ingress.project.services.DriveLicenseValidator;
import az.ingress.project.services.DriveLicenseDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class ProjectApplication implements CommandLineRunner {

	private final DriveLicenseValidator driveLicenseValidator;

	@SneakyThrows
	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class);
	}

	@Override
	public void run(String... args) throws Exception {

		log.info("Started");
		DriveLicenseDto dto = new DriveLicenseDto();
		dto.setId(2L);
		dto.setExpireDate(new Date());
		dto.setCountry("Russia");
		dto.setCategory("B");
		System.out.println(driveLicenseValidator.isValid(dto));

		DriveLicenseDto dto1 = new DriveLicenseDto();
		dto1.setId(2l);
		dto1.setExpireDate(new Date());
		dto1.setCountry("Azerbaijan");
		dto1.setCategory("C");
		System.out.println(driveLicenseValidator.isValid(dto1));
	}
}
