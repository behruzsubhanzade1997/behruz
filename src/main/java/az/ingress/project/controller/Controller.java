package az.ingress.project.controller;

import az.ingress.project.services.HelloServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/hello")
public class Controller {
    private final HelloServiceImpl service;

    @GetMapping("/1")
    public Dto sayHello(){
        return service.sayHello();
    }
    @GetMapping("/2")
    public Dto sayHello2(){
        return service.sayHello();
    }
    @GetMapping("/3")
    public Dto sayHello3(){
        return service.sayHello();
    }
}
